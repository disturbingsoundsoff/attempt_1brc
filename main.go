package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

type Data struct {
    sum float64
    count int
    max float64
    min float64
    minSet bool
}

func main(){
    file, err := os.Open("measurements_1_mil.txt")
    if err != nil {
        log.Fatal(err)
        os.Exit(1)
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)
    cityTempMap := make(map[string]Data)

    for scanner.Scan() {
        city, tempString, _ := strings.Cut(scanner.Text(), ";")
        temp, _ := strconv.ParseFloat(tempString, 64)
        
        data := cityTempMap[city]
        data.sum += temp
        data.count++

        if !data.minSet {
            data.max = temp
            data.min = temp
            data.minSet = true
        } else if temp > data.max { 
            data.max = temp 
        } else if temp < data.min { 
            data.min = temp 
        }

        cityTempMap[city] = data
    }

    
    
}

